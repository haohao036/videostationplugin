# videostationplugin

#### 关于一些安装时遇到的问题
1.  不要修改压缩包的名称,否则会显示文件损坏
2.  确认压缩包是否已经下载完成,如未完成会显示文件损坏
3.  确认网络是否能访问https://m.douban.com 和 https://movie.douban.com 两个地址,否则插件无法使用
4.  videostation版本要求  DSM 7.0需要3.0.0以上, DSM 6.0需要2.5.0以上

#### qq群：874179228

#### 介绍
群晖videostation插件 豆瓣源

#### 一、压缩包直接安装
[com.douban.tmdb.zip](https://gitee.com/hexdgit/videostationplugin/raw/master/com.douban.tmdb.zip)


#### 二、源码安装
 打包 7z a com.douban.tmdb.zip com.douban.tmdb






